package com.simplilearn.stepdefs;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.Assert;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions
public class DemoDefinitions {

/*   --- this will force an undefined state ----
    @Given("I want to demonstrate an undefined test")
    public void i_want_to_demonstrate_an_undefined_test() {
    }
*/

    @When("I have an Undefined Test")
    public void i_have_an_undefined_test() {
    }

    @Then("This test will show as undefined when ran")
    public void this_test_will_show_as_undefined_when_ran() {
    }

    @Given("I want to demonstrate a passing test")
    public void i_want_to_demonstrate_a_passing_test() {
        Assert.assertTrue(true);
    }

    @Then("The Test will show as Passing when ran")
    public void the_test_will_show_as_passing_when_ran() {
        Assert.assertTrue(true);
    }

    @Given("I want to demonstrate a failing test")
    public void i_want_to_demonstrate_a_failing_test() {
        Assert.assertTrue(false);
    }

    @When("I perform a Verification")
    public void i_perform_a_verification() {
    }

    @Then("The Test will show as Failing when ran")
    public void the_test_will_show_as_failing_when_ran() {
        Assert.assertTrue(false);
    }
}

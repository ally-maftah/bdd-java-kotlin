Feature: Undefined, Passed, and Failed

  Scenario: Demo os Undefined Test
    Given I want to demonstrate an undefined test
    When I have an Undefined Test
    Then This test will show as undefined when ran

  Scenario: Demo a Passing Test
    Given I want to demonstrate a passing test
    When I perform a Verification
    Then The Test will show as Passing when ran

  Scenario: Demo a Failing Test
    Given I want to demonstrate a failing test
    When I perform a Verification
    Then The Test will show as Failing when ran
